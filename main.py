import sys
from datetime import datetime
from typing import Union, Optional
from urllib import parse

from core.errors import InvalidInput
from github import MostActiveContributors, PullRequests, Issues


def get_datetime_from_input(date: Union[None, str]) -> Union[None, datetime]:
    if date:
        try:
            return datetime.strptime(date, "%Y-%m-%d")
        except ValueError:
            raise InvalidInput("Try set date in YYYY-MM-DD format")
    return None


def get_repo_owner_from_url(url: str) -> (str, str):
    try:
        path = parse.urlparse(url).path.split("/")
        return path[1], path[2]
    except IndexError:
        raise InvalidInput("Invalid URL, can't parse repository owner and repository name")


def get_arg_value(index, required: bool = False) -> Optional[str]:
    try:
        return sys.argv[index]
    except IndexError:
        if required:
            raise InvalidInput("Not set required positional argument")
        return None


def main():
    owner, repo = get_repo_owner_from_url(get_arg_value(1, required=True))
    start_date = get_datetime_from_input(get_arg_value(3))
    end_date = get_datetime_from_input(get_arg_value(4))
    params = {
        'owner': owner,
        'repo': repo,
        'start_date': start_date,
        'end_date': end_date,
        'branch': get_arg_value(2),
    }
    MostActiveContributors(**params).print()
    PullRequests(**params).print()
    Issues(**params).print()


if __name__ == '__main__':
    args = sys.argv
    main()
