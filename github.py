from datetime import datetime
from typing import Dict

from core.api import BaseGithubRequest, print_decorator


class DateMixin:
    def check_created_at(self, created_at: datetime) -> bool:
        start = True
        end = True
        if self.start_date and created_at < self.start_date:
            start = False
        if self.end_date and created_at > self.end_date:
            end = False
        return all((start, end))


class MostActiveContributors(BaseGithubRequest):
    TITLE = "Most active contributors"
    API_PATH = "/repos/{owner}/{repo}/commits"

    @print_decorator
    def print(self):
        contributors: Dict[str, int] = {}
        for commit in self.data:
            if author := commit.get('author'):
                author_login: str = author.get('login')
                if author_login not in contributors.keys():
                    contributors[author_login] = 1
                contributors[author_login] += 1
        print("{:<20} {:<10}".format('Login', 'Number of commits'))
        sorted_contributors = {k: v for k, v in sorted(contributors.items(), key=lambda item: item[1], reverse=True)}
        for login, number_of_commits in sorted_contributors.items():
            print("{:<20} {:<10}".format(login, number_of_commits))

    def params(self):
        params = {
            'sha': self.branch,
        }
        if self.start_date:
            params['since'] = self.start_date.strftime("%Y-%m-%dT%H:%M:%S%z")
        if self.end_date:
            params['until'] = self.end_date.strftime("%Y-%m-%dT%H:%M:%S%z")
        return params


class PullRequests(DateMixin, BaseGithubRequest):
    TITLE = "Pull requests"
    API_PATH = "/repos/{owner}/{repo}/pulls"

    def params(self):
        return {
            'state': 'all',
            'base': self.branch,
        }

    @print_decorator
    def print(self):
        data = {
            'open': 0,
            'closed': 0,
            'old': 0
        }
        for pull_request in self.data:
            pr_created_at = datetime.strptime(pull_request['created_at'][:-1], "%Y-%m-%dT%H:%M:%S")
            if not self.check_created_at(pr_created_at):
                continue
            if pull_request.get('state') == 'closed':
                data['closed'] += 1
                continue
            if pull_request.get('state') == 'open':
                if (datetime.now() - pr_created_at).days >= 30:
                    data['old'] += 1
                else:
                    data['open'] += 1
        for result_name, number_of_pull_requests in data.items():
            print(f"\t{result_name}: {number_of_pull_requests}")


class Issues(DateMixin, BaseGithubRequest):
    TITLE = "Issues"
    API_PATH = "/repos/{owner}/{repo}/issues"

    def params(self):
        return {
            'state': 'all',
        }

    @print_decorator
    def print(self):
        data = {
            'open': 0,
            'closed': 0,
            'old': 0
        }
        for issue in self.data:
            issue_created_at = datetime.strptime(issue['created_at'][:-1], "%Y-%m-%dT%H:%M:%S")
            if not self.check_created_at(issue_created_at):
                continue
            if issue.get('state') == 'closed':
                data['closed'] += 1
                continue
            if issue.get('state') == 'open':
                if (datetime.now() - issue_created_at).days >= 14:
                    data['old'] += 1
                else:
                    data['open'] += 1
        for result_name, number_of_issues in data.items():
            print(f"\t{result_name}: {number_of_issues}")
