FROM python:3.8-alpine
WORKDIR /app

RUN pip3 install requests==2.25.1

COPY . /app