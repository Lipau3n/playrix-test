# Анализ репозитория GitHub

## Параметры:
1. URL публичного репозитория на github.com (обязательное)
2. Ветка репозитория (необязательно, по умолчанию — master)
3. Дата начала анализа (необязательно, по умолчанию неограничено)
4. Дата окончания анализа (необязательно, по умолчанию неограничено)

## Пример использования
```
python3 main.py https://github.com/SmileyChris/django-countries master 2020-12-01 2020-12-30
```

## Использование вместе с Docker
1. docker build -t playrix-test .
2. docker run -it --rm playrix-test python3 main.py https://github.com/SmileyChris/django-countries master 2020-12-01 2020-12-30

## Вариант реализации CI/CD
В момент пуша, можно создавать образ для докера и хранить его на сервере docker registry.  
А в момент деплоя выполнять на сервере `docker pull`