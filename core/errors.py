class InvalidInput(Exception):
    pass


def print_api_error(status_code: int, text: str):
    print(f"Error with status code {status_code}, message: {text}")
