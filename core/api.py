from datetime import datetime
from time import sleep
from typing import Dict, Union, List

import requests

from core.errors import print_api_error

BASE_URL = "https://api.github.com"


class BaseGithubRequest:
    TITLE: str  # Название анализа
    API_PATH: str  # GitHub API path

    def __init__(self, owner: str, repo: str, start_date: datetime = None, end_date: datetime = None,
                 branch: str = 'master') -> None:
        """
        :param owner: Логин владельца репозитория
        :param repo: Название репозитория
        :param start_date: Дата начала анализа
        :param end_date: Дата окончания анализа
        :param branch: Название ветки репозитория, по умолчанию master
        """
        super().__init__()
        self.owner = owner
        self.repo = repo
        self.start_date = start_date
        self.end_date = end_date
        self.branch = branch

        self.data = []

    @property
    def headers(self) -> Dict:
        return {
            "Accept": "application/vnd.github.v3+json",
        }

    @property
    def api_url(self) -> str:
        path = self.API_PATH.format(owner=self.owner, repo=self.repo)
        return BASE_URL + path

    def get(self, page: int = 0) -> Union[List[Dict], List]:
        params = self.params()
        params.update(page=page, per_page=100)
        res = requests.get(self.api_url, headers=self.headers, params=params)
        if res.status_code == 200:
            response_data = res.json()
            self.data += response_data
            if len(response_data) == 100:
                self.get(page + 1)
        elif res.status_code == 403 and res.headers.get('X-RateLimit-Remaining') == '0':
            self.pause(res, page)
        elif res.status_code != 404:
            print_api_error(res.status_code, res.text)
        return self.data

    def pause(self, response: requests.Response, page: int):
        """
        Если мы достигнем лимита на количество запросов, то выполним остановку скрипта до времени,
        пока нам снова не будут доступны запросы к API GitHub и продолжим его выполнение с предыдущего места
        """
        now = datetime.now().timestamp()
        reset_timestamp = int(response.headers.get('X-RateLimit-Reset'))
        time_to_sleep = reset_timestamp - now
        print(f"GitHub API request limit exceeded. Continue after {time_to_sleep} seconds")
        sleep(time_to_sleep)
        self.get(page=page)

    def params(self):
        return {}

    def print(self):
        raise NotImplementedError()


def print_decorator(func):
    """
    Декоратор проверяет данные полученные из API, если они есть, то функция их покажет,
    если данных нет, то функция не выполнится.
    """

    def print_result(this: BaseGithubRequest):
        print(f"{this.TITLE}:")
        if this.get():
            func(this)
        else:
            print("No data")
        print("\n")

    return print_result
